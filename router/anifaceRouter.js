import express from 'express';
import { model1Post, model2Post, model3Post, model4Post } from '../controller/anifaceController.js';

const anifaceRouter = express.Router();

anifaceRouter.post('/model1/inference', model1Post);
anifaceRouter.post('/model2/inference', model2Post);
anifaceRouter.post('/model3/inference', model3Post);
anifaceRouter.post('/model4/inference', model4Post);

export default anifaceRouter;
