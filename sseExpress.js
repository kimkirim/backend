import express from 'express';

var sendInterval = 3000;

var userNames = ["John", "Monica", "Martin", "Rose", "James", "Tom"];
var randomMessages = ["I'm fine", "I'll meet you in 10 minutes", "I'll call you back later", "I'm busy"];

var intervalID = null;

const app = express();
const PORT = 8080
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

function sendServerSendEvent(req, res) {
  console.log("Connected")
  res.writeHead(200, {
    'Content-Type' : 'text/event-stream',
    'Cache-Control' : 'no-cache',
    'Connection' : 'keep-alive'
  });
  
  res.write('\n');

  intervalID = setInterval(function() {
    var eventName = null;
    var eventData = randomMessages[getRandom(0,randomMessages.length)]

    if (getRandom(0,2) == 0) {
      eventName = "user-connected";
      eventData = userNames[getRandom(0,userNames.length)]
    }

    console.log('event: ' + eventName);
    console.log('data:  ' + eventData + '\n');

    writeEvent(res, eventName, eventData);

  }, sendInterval);
}

function writeEvent(res, eventName, data) {
    var payload = ""

    if (eventName) {
      payload += 'event: ' + eventName + '\n'; 
    }

    payload += 'data: ' + data + '\n\n';

    res.write(payload);
    // res.send(payload);
}

function getRandom(min, max) {
  return Math.floor(Math.random() * (max - min) + min);
}

app.post('/sse', async (req, res) => {
  console.log(req.body)
  if (req.headers.accept && req.headers.accept == 'text/event-stream'){
    req.on('close', function(){
        clearInterval(intervalID);
    });

    sendServerSendEvent(req, res);
  }else{
    // res.writeHead(404);
    res.sendStatus(404);
  }
});

app.use((req, res, next) => {
  return res.sendStatus(400);
});

app.use((error, req, res, next) => {
  console.error(error);
  return res.sendStatus(500);
});

app.listen(PORT, () =>
  console.log(`success connect! http://localhost:${PORT}`)
);